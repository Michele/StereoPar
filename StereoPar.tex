\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[hidelinks]{hyperref}
\usepackage{graphicx}
\usepackage{fullpage}
\usepackage{float}
\title{Notes on the geometrical computation of core parameter and height of a Cherenkov shower as seen by an array of telescopes}
\date{29/01/2016}
\author{Michele Mastropietro}

\newcommand{\vect}[1]{\mathbf{#1}}
\newcommand{\versor}[1]{\hat{\mathbf{#1}}}

\begin{document}
\maketitle

\section{Reference frames}
We define two reference frames whose origin is the same and located at the center of the main mirror (reflector):
% \begin{itemize}
%  \item Celestial (TODO, unneded here )
%   \subitem $h$ hour angle %TODO
%   \subitem $\delta$ declination


\begin{figure}[h!]
 \centering
  \includegraphics{LocCoord.pdf}
  \caption{Local coordinate system}
  \label{fig:loc_coord}
\end{figure}
 
 \paragraph{Local reference frame} (superscript $loc$) is defined as the CORSIKA reference frame which is described in figure~\ref{fig:loc_coord}. The $x$ axis points towards North, $y$ axis towards West and $z$ towards Zenith (direction $\versor{z}$). Azimuth, as ususal, starts from North and increases clockwise: North, East, Sud, West.
 \begin{itemize}
  \item $\theta$ Zenith.
  \item $\phi$ Azimuth starting from the geographical North direction
\end{itemize}

\paragraph{Camera reference frame}
\begin{figure}[h!]
\centering
  \includegraphics{CameraSys.pdf}
  \caption{Camera system}
  \label{fig:camera_frame}
\end{figure}

(superscript $cam$) whose $x$ and $y$ axes lie on the plane perpendicular to the pointing direction of the telescope, $\versor{r}_0$; the $z$ axis points in the opposite direction w.r.t. $\versor{r}_0$. For convenience, the focal plane (where the camera lies) is put at a distance $1$ from the reflector. The axes of this camera reference frame are illustrated in figure~\ref{fig:camera_frame}, that is when the observer looks from the reflector to the pointing direction of the telescope, the $x$ axis points towards the right parallel to the horizon, the $y$ axis points upwards and the $z$ axis towards the reflector. More formally the axes direction are the following:
\begin{equation*}
 \versor{e}^{(cam)}_x = \frac{\versor{r}_0 \times \versor z}{|\versor{r}_0 \times \versor z|}, \qquad
 \versor{e}^{(cam)}_y = \versor{e}^{(cam)}_x \times \versor{r}_0, \qquad
 \versor{e}^{(cam)}_z = -\versor{r}_0.
\end{equation*}
In this reference frame the direction in the sky of a point in the focal plane is decribed by:
\begin{equation}
\hat{\mathbf{r}}^{(cam)} = \frac{1}{\sqrt{1+x^2_c+y^2_c}}
\begin{pmatrix}
 x_c\\y_c\\-1
\end{pmatrix}
\label{eq:fict_vec}
\end{equation}
where $x_c$ and $y_c$ are the coordinates of a point in the camera adimensionalized with the focal length $f_l$.
By definition the center of the camera $(x_c,y_c) = (0,0)$ corresponds to the direction $(\theta_0, \phi_0)$ the telescope is pointing to. 
For ASTRI $f_l = 2150$ mm so that a true position on the camera can be found:
\begin{equation}
\begin{array}{l} 
  x_c^{true} = f_l \,x_c \\
  y_c^{true} = f_l \,y_c
\end{array}
\label{eq:true_cam}
\end{equation}

\subsection{From camera to local coordinates}
The transformation matrix $A$ (see appendix \ref{matrixA}) from the fictive camera reference system to the local reference system is 
\[ A =
\begin{pmatrix}
 -\sin \phi_0 & -\cos \theta_0 \cos \phi_0 & -\sin \theta_0 \cos \phi_0 \\
 -\cos \phi_0 &  \cos \theta_0 \sin \phi_0 &  \sin \theta_0 \sin \phi_0 \\
  0         &  \sin \theta_0           & -\cos \theta_0
\end{pmatrix}
\]
So that
\begin{equation}
\vect{r}^{(loc)} = A \, \vect{r}^{(cam)}
\label{eq:cam2loc}
\end{equation}

\subsection{Useful relations from local coordinates to camera coordinates}
From figure \ref{fig:loc_coord} the components of a generic direction $\versor{r}$ in the local reference frames can be written as:
\begin{equation}
\versor{r}^{(loc)}=
\begin{pmatrix}
 \sin \theta \cos \phi\\
 -\sin \theta \sin \phi\\
 \cos \theta
\end{pmatrix}
\end{equation}
Inverting the equation \eqref{eq:cam2loc} we can compute $\versor{r}^{(cam)}$:
\begin{equation*}
\versor{r}^{(cam)} = A^{-1}\, \versor{r}^{(loc)} = A^T \, \versor{r}^{(loc)}= 
\begin{pmatrix}
 \sin\theta (\sin\phi \cos\phi_0 - \cos \phi \sin\phi_0) \\
 \sin\theta_0 \cos\theta - \cos\theta_0\sin\theta (\cos\phi_0 \cos\phi + \sin\phi_0 \sin\phi) \\
 -\cos\theta_0 \cos\theta - \sin\theta_0\sin\theta (\cos\phi_0 \cos\phi + \sin\phi_0 \sin\phi)
\end{pmatrix}
\end{equation*}

\begin{equation}
\versor{r}^{(cam)} = 
\begin{pmatrix}
 \sin\theta \sin(\phi -\phi_0)\\
 \sin\theta_0 \cos\theta - \cos\theta_0\sin\theta \cos(\phi -\phi_0)\\
 -\cos\theta_0 \cos\theta - \sin\theta_0\sin\theta \cos(\phi -\phi_0)
\end{pmatrix}
\end{equation}
These are the components of the vector $\versor{r}$ in the camera system.

Remember that ASTRI behaves like a Cassegrain telescope in which we have a reflection both right-left and up-down. This is because the separation from the two mirrors is less than the focal lengths of the secondary and the primary and the secondary mirror is inside the focus of the primary \cite{Rodeghiero}.

This implies that the $x$ and $y$ components of $\versor{r}^{(cam)}$ change sign (or, equivalently, $\versor{r}^{(cam)}$ undergo a rotation of $180 \deg$ around  $\versor{r}_0$):

\begin{equation*}
\versor{r}^{(cam)}_{mirror} = 
\begin{pmatrix}
 -\sin\theta \sin(\phi -\phi_0)\\
 -\sin\theta_0 \cos\theta - \cos\theta_0\sin\theta \cos(\phi -\phi_0)\\
 -\cos\theta_0 \cos\theta - \sin\theta_0\sin\theta \cos(\phi -\phi_0)
\end{pmatrix} =
\begin{pmatrix}
r_x\\ r_y\\r_z
\end{pmatrix}
\end{equation*}
By intersecting the direction $\versor{r}^{(cam)}_{mirror}$ with the plane $z=-1$ of the camera reference frame, we can obtain the coordinate in the focal plane $(x_c, y_c)$:

\begin{equation*}
\tau \, \versor{r}^{(cam)}_{mirror} =
\begin{pmatrix}
 x_c\\  y_c\\  -1
\end{pmatrix}
\end{equation*}
By solving the following set of equations
\[
\begin{cases}
\tau \, r_x = x_c \\
\tau \, r_y = y_c \\
\tau \, r_z = -1 \\
 r_x^2 + r_y^2 + r_z ^ 2 = 1
\end{cases}
\]
the coordinate in the focal plane $(x_c, y_c)$ are:
\begin{align*}
  x_c &= \frac{ -\sin\theta \sin(\phi -\phi_0)}{\cos\theta_0 \cos\theta + \sin\theta_0 \sin\theta \cos(\phi -\phi_0)}\\
  y_c &= \frac{ -\sin\theta_0 \cos\theta + \cos\theta_0 \sin\theta \cos(\phi -\phi_0)}{\cos\theta_0 \cos\theta + \sin\theta_0\sin\theta \cos(\phi -\phi_0)}
\end{align*}
Remember to use equation \eqref{eq:true_cam} to obtain the true quantity coordinates in mm.

\section{Method to find core position}
Core position is the intersection between the shower direction and the plane of the telescope array. In other words it is the point on the ground where the shower would fall.
\subsection{Finding the line from telescope to the core position}
Taking Fig. \ref{fig:core} as a reference, in the plane $z=0$ we need the lines $r_i$ which are pointing to the shower core $(x^*,y^*)$ from the telescopes positions $(x^t,y^t)$. 
These lines result from the intersection of $z=0$ plane and the plane $\pi$ which is defined by the shower axis (as seen by the telescope) and the telescope position.
For one telescope, the situation is illustrated below
\begin{figure}[h!]
\label{fig:core}
\centering
\includegraphics{StereoCore.pdf}
\caption{Notation for core position reconstruction}
\end{figure}

Plane $\pi$ is defined as
\[
\pi: n_x(x-x^t)+n_y(y-y^t)+n_z(z-z^t) = 0 
\]
where
\begin{equation}
\versor{r} = \versor{d} \times \versor{c}=
\begin{pmatrix}
 n_x\\n_y\\n_z
\end{pmatrix}
=
\begin{pmatrix}
 d_y c_z - d_z c_y \\
 d_z c_x - d_x c_z \\
 d_x c_y - d_y c_x
\end{pmatrix}
\label{eq:d_cross_c}
\end{equation}

The versors $\hat c$ and $\hat d$ are defined as pointing from the telescope position to respectively a point in the sky correspondent to the center of gravity $C$ of the image, and a point $D$ shifted along the shower axis.
These two points in the camera reference frame can be defined as:
\[
\begin{array}{l}
C : (\bar x,\bar y)^{(cam)} \\ [1ex]
D : (\bar x + \cos\delta,\bar y + \sin \delta)^{(cam)} 
\end{array}
\]
where $\delta$ is the angle of the shower as defined by \cite{Weekes}. %TODO
These two points determine the direction $\hat n = \hat d \times \hat c$ in a local reference frame.
All local systems (one for each telescope) share the same pair of axis $xy$. 

The result of the system
\[
\left\{
\begin{array}{l} 
  n_x(x-x^t)+n_y(y-y^t)+n_z(z-z^t) = 0 \\ [1ex]
  z=0 
\end{array}
\right. 
\]
is the line $r$ defined as:
\[
y = \underbrace{-\frac{n_x}{n_y}}_a x+\underbrace{\frac{n_x x^t+n_y y^t+n_z z^t}{n_y}}_b
\]
\subsection{Finding the core position with many telescopes}
\label{sec:find_core}
At this point, for each telescope we have a line $r_i$ with $i=1,\ldots,n_{tel}$. The likely core position $(x,y)$ is the point which minimizes the sum of the distances from all $r_i$.
Some (to be defined) weights $w_i$ can be introduced for each telescope to take into account different image quality.
Weights should be defined so as this holds:
\[ \sum_{i=1}^{n_{tel}}  w_i =1 \]

We want to minimize the function:
\[ f(x,y) = \sum_{i=1}^{n_{tel}} d_i^2 w_i \]
where $ d_i $ is the distance between the line $r_i: a_i x - y + b_i = 0 $ representing the direction of the shower seen by telescope $ i $ and the point $ x, y $:
\[ d_i = \frac{|a_ix-y+b_i|}{\sqrt{a_i^2 + 1}}\]
So:
\[ f(x,y) = \sum_{i=1}^{n_{tel}} w_i\frac{(a_ix-y+b_i)^2}{a_i^2 + 1} \]
\subsection{Computation of the minimum and solution}
\label{sec:computations}
The function $f$ is convex so we need only to compute partial derivatives:
\[
\frac{\partial f}{\partial x} = \sum_{i=1}^{n_{tel}} 2 w_i a_i \frac{a_ix-y+b_i}{a_i^2 + 1} =
2\left(\sum_{i=1}^{n_{tel}} \frac{a_i^2}{a_i^2 + 1} w_i\right) x\, - 2\left(\sum_{i=1}^{n_{tel}} \frac{a_i}{a_i^2 + 1} w_i\right) y +
2\left(\sum_{i=1}^{n_{tel}} \frac{a_i b_i}{a_i^2 + 1} w_i\right)
\]

\[ 
\frac{\partial f}{\partial y} = \sum_{i=1}^{n_{tel}} -2w_i\frac{a_ix-y+b_i}{a_i^2 + 1} =
- 2\left(\sum_{i=1}^{n_{tel}} \frac{a_i}{a_i^2 + 1} w_i\right) x + 2\left(\sum_{i=1}^{n_{tel}} \frac{1}{a_i^2 + 1} w_i\right) y
- 2\left(\sum_{i=1}^{n_{tel}} \frac{b_i}{a_i^2 + 1} w_i\right)
\]
Putting derivatives to zero, solve the linear system:
\[
\left\{ 
\begin{array}{l}
  p_1 x + q_1 y + r_1 = 0\\[2ex]
  p_2 x + q_2 y + r_2 = 0
\end{array}\right.
\]
where:
\[
p_1 = \sum_{i=1}^{n_{tel}} \frac{a_i^2}{a_i^2 + 1}w_i,\quad
q_1 = -\sum_{i=1}^{n_{tel}} \frac{a_i}{a_i^2 + 1}w_i,\quad
r_1 = \sum_{i=1}^{n_{tel}} \frac{a_i b_i}{a_i^2 + 1}w_i
\]

\[
 p_2 = \sum_{i=1}^{n_{tel}} \frac{a_i}{a_i^2 + 1}w_i,\quad
 q_2 = -\sum_{i=1}^{n_{tel}} \frac{1}{a_i^2 + 1}w_i,\quad
 r_2 = \sum_{i=1}^{n_{tel}} \frac{b_i}{a_i^2 + 1}w_i
\]
 
The solution (e.g. by Cramer's rule) is:
\[
\left\{
\begin{array}{l} 
  x = x^* = \dfrac{q_1 r_2 - q_2 r_1}{p_1 q_2 - p_2 q_1} \\ [3ex]
  y = y^* = \dfrac{p_2 r_1 - p_1 r_2}{p_1 q_2 - p_2 q_1} 
\end{array}
\right. 
\]



\subsection{Summary}
For each telescope:
\begin{enumerate}
 \item Find the points $C$ and $D$ in the camera with their coordinates. 
 \item Find the two directions $ \versor{c}^{(cam)}$ $ \versor{d}^{(cam)}$ in the camera reference frame using equation \eqref{eq:fict_vec} and \eqref{eq:true_cam}.
 \item Use equation \eqref{eq:cam2loc} to transform to the local reference frame   
 \item Use equation \eqref{eq:d_cross_c} to compute $\versor n$.
\end{enumerate}
At the end:
\begin{enumerate}
 \item[5] Use algorithm in section \ref{sec:find_core} and \ref{sec:computations} to compute $x^*,y^*$
\end{enumerate}

\section{Computation of the height of the shower}
 We start finding the direction $\versor v$ which for each telescope points to the center of gravity ($C$) of the image.
 \begin{figure}[h!]
\centering
  \includegraphics{HeightEstimation.pdf}
  \caption{Notation for height estimation}
  \label{fig:height_est}
\end{figure}

Let $\psi$ be a plane parallel to the ground, see figure \ref{fig:height_est}. Each direction $\versor{v}_i$ will intersect $\psi$ in the point $(x_i, y_i)$.
We want to find the height $z$ of the plane $\psi$ for which the mean squared distance of the points from their barycenter is minimal.

We want to minimize the function:
\[ f(z) = \sum_{i=1}^N \dfrac{(x_i -\bar x)^2 + (y_i -\bar y)^2}{N}, \qquad \text{with } \bar x = \frac 1 N \sum_{i=1}^N x_i \quad \text{and} \quad \bar y = \frac 1 N \sum_{i=1}^N y_i\]
where $N$ is the number of telescopes and $(\bar x,\bar y)$ are the coordinates of the barycenter of the set of intersection points $(x_i, y_i)$.
% and $x_i$ and $y_i$ for the telescope $i$ are the coordinates on the plane $\psi$ of the point resulting from the intersection between the direction $\hat{v_i}$ and the plane $\psi$. 

To compute $x_i$ and $y_i$, with reference to figure \ref{fig:height_est}, it is possible to use the relations
\[
\Delta z = \Delta x \tan \eta_x = \Delta x \frac{\hat v_z}{\hat v_x}, \qquad 
\Delta z = \Delta y \tan \eta_y = \Delta y \frac{\hat v_z}{\hat v_y}
\]
which imply
\begin{equation*}
\begin{array}{l}
\Delta x = x_i - x^t_i = (z - z^t_i) \dfrac{\hat v_{ix}}{\hat v_{iz}}\\[2ex]
x_i = z \dfrac{\hat v_{ix}}{\hat v_{iz}} - z^t_i \dfrac{\hat v_{ix}}{\hat v_{iz} } + x^t_i
\end{array}
\end{equation*}
and analogously
\[
y_i = z \frac{\hat v_{iy}}{\hat v_{iz}} - z^t_i \frac{\hat v_{iy}}{\hat v_{iz}} + y^t_i
\]
or, with a simplified notation
\begin{equation}
\begin{array}{l}
  x_i = a_i z + b_i\\
  y_i = c_i z + d_i
\end{array}
\end{equation}
where:
\begin{equation}
\begin{array}{l}
  a_i = \dfrac{\hat v_{ix}}{\hat v_{iz}} \\ [2ex]
  b_i = - z^t_i \dfrac{\hat v_{ix}}{\hat v_{iz}} + x^t_i \\ [2ex]
  c_i = \dfrac{\hat v_{iy}}{\hat v_{iz}} \\ [2ex]
  d_i = - z^t_i \dfrac{\hat v_{iy}}{\hat v_{iz}} + y^t_i \\ [2ex]
\end{array}
\end{equation}

Taking the derivative of $f(z)$:
\begin{equation}
\begin{array}{rl} \displaystyle
f'(z) = &\dfrac 2 N \displaystyle\sum_{i=1}^N \left[ (x_i-\bar x) (x_i-\bar x)' + (y_i-\bar y)(y_i-\bar y)'\right]\\ [3ex]
      = &\dfrac 2 N \displaystyle\sum_{i=1}^N \left[ \left( a_i z + b_i - \dfrac 1 N  \displaystyle\sum_{i=1}^N (a_i z + b_i)\right)(a_i -\bar a) 
      + \left( c_i z + d_i - \dfrac 1 N  \displaystyle\sum_{i=1}^N (c_i z + d_i)\right)(c_i -\bar c) \right] =\\[3ex]
      = &\dfrac 2 N \displaystyle\sum_{i=1}^N \left[ z (a_i - \bar a)^2 + (b_i - \bar b) (a_i - \bar a) + z (c_i - \bar c)^2 + (d_i - \bar d)(c_i - \bar c) \right]  \\ [4ex]
      = &\dfrac 2 N \displaystyle\sum_{i=1}^N \left[ z \left( (a_i - \bar a)^2 + (c_i - \bar c)^2 \right) + (b_i-\bar b)(a_i-\bar a) + (d_i-\bar d)(c_i-\bar c) \right] \\ [4ex]
      = &z \dfrac 2 N \displaystyle\sum_{i=1}^N \left( (a_i - \bar a)^2 + (c_i - \bar c)^2 \right) + \dfrac 2 N \displaystyle\sum_{i=1}^N \left((b_i-\bar b)(a_i-\bar a) + (d_i-\bar d)(c_i-\bar c) \right)
\end{array}
\end{equation}
% Which leads to the equation
% \begin{equation}
%  \left( \sum_{i=1}^N ( a_i -\bar a )^2 + \sum_{i=1}^N ( c_i -\bar c )^2 \right) z + \sum_{i=1}^N (b_i-\bar b)(a_i-\bar a) + \sum_{i=1}^N (d_i-\bar d)(c_i-\bar c)= 0
% \end{equation}
So the solution of the equation $f'(z) = 0$ is:
\begin{equation} 
 z = -\dfrac{  \displaystyle\sum_{i=1}^N \left( (b_i-\bar b)(a_i-\bar a) + (d_i-\bar d)(c_i-\bar c) \right) }{ \displaystyle\sum_{i=1}^N \left( (a_i -\bar a)^2 + (c_i -\bar c)^2 \right) }
\end{equation}

\section{Open points}
\begin{itemize}
 \item Impact of the double mirror (is it equivalent to no reflection?)
 %\item Camera system rotates, $x$ axis is not always parallel to the ground. Where this should be taken into account?
 \item conversion to Corsika system. Is it different from the local shown? More precisely, where does the azimuth start from in real data?
 \item Check whether different height of the telescopes can be a problem for some algorithm.
\end{itemize}

\appendix
\section{Derivation of the matrix for coordinates transformation}
\label{matrixA}
 Columns of matrix $A$ (which allows to transform from camera fictive reference system to local reference system) are the representation of the canonical base of the fictive camera reference system $(\mathbf{e}_x,\mathbf{e}_y,\mathbf{e}_z)$ in the local coordinate system. % With reference to figure \ref{fig:coord_trasf}
The following relations are useful, being $\phi = \delta + \frac{3}{2}\pi$:
 \begin{equation}
\begin{array}{l}
  \cos \delta = -\sin \phi \\
  \sin \delta =  \cos \phi
\end{array}
\end{equation}
 \begin{figure}[h!]
\centering
  \includegraphics{ConversionCoordCorsika.pdf}
  \caption{Canonical base of the camera fictive reference system as seen from local reference frame}
  \label{fig:coord_trasf}
\end{figure}
\[\mathbf{e}_x =
 \begin{bmatrix}
  \cos\delta \\ -\sin\delta \\ 0
 \end{bmatrix}=
 \begin{bmatrix}
  -\sin\phi \\ -\cos\phi \\ 0
 \end{bmatrix},
 \quad 
 \mathbf{e}_y =
  \begin{bmatrix}
  -\cos \theta \sin\delta \\ -\cos\theta \cos\delta \\ \sin\theta
 \end{bmatrix} =
 \begin{bmatrix}
  -\cos \theta \cos\phi \\ \cos\theta \sin\phi \\ \sin\theta
 \end{bmatrix},
 \quad
 \mathbf{e}_z =
 \begin{bmatrix}
  -\sin\theta \sin\delta \\ -\sin\theta \cos\delta \\ -\cos\theta
 \end{bmatrix}=
 \begin{bmatrix}
  -\sin\theta \cos\phi \\  \sin\theta \sin\phi \\ -\cos\theta
 \end{bmatrix}
\]

\[ A =
\begin{pmatrix}
 -\sin \phi & -\cos \theta \cos \phi & -\sin \theta \cos \phi \\
 -\cos \phi &  \cos \theta \sin \phi &  \sin \theta \sin \phi \\
  0         &  \sin \theta           & -\cos \theta
\end{pmatrix}
\]

\begin{thebibliography}{9}

\bibitem{Rodeghiero}
  Gabriele Rodeghiero, Personal communication, 7/9/2015.
\bibitem{Weekes}
  Weekes, T. C., Cawley, M. F., Fegan, D. J., et al. \emph{Observation of TeV Gamma Rays from the Crab Nebula using the Atmospheric Cerenkov Imaging Technique} 1989, ApJ, 342, 379
\end{thebibliography}

% \bibliography{}
% 
%  Rodeghiero, Personal communication -7 Settembre 2015
% % In ASTRI la separazione tra i due specchi e' inferiore alla somma delle
% focali del primario e del secondario e lo specchio secondario e' dentro al
% fuoco del primario, quindi il telescopio si comporta come i due sistemi
% sopracitati.
\end{document}